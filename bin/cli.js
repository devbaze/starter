#!/usr/bin/env node
const actions = require('../src/main');
const programName = process.argv[1].split('/').pop();
let asked = process.argv[2];
if(process.argv.length < 3){
	console.error(`\nUsage : ${programName} action [options...]`);
	asked = 'help';
}
if(actions[asked]) {
	(async function(){
		try{
			await actions[asked](process.cwd(), process.argv[3], process.argv[4]);
		}
		catch (e){console.error(e);}
	})();
} else console.error(`Hcon : unknow command "${asked}".\nType "${programName} help" for more.`);


const updateNotifier = require('update-notifier');
const pkg = require('../package.json');
const notifier = updateNotifier({pkg,updateCheckInterval:60*1000,callback:(err,update)=>{
	if(update && update.type && update.type !== 'latest') {
		notifier.update = update;
		notifier.notify({defer: false});
	}
}});

/*
si update : TODO: met à jour le fichier .gitlab-ci.yml
  - régénère le PROJECT_NAME en fonction de celui indiqué dans package.json
  - régénère la partie build en fonction des visibilité de data.yml (public & autre)
  - régénère la partie publish en fonction de publish-targets.yml


si build :
  - TODO: filtre le data.yml copié pour ne laisser que les info publiques (TODO: ajouter ça)
  - TODO: faire de même pour chaque niveau d'accès présent dans data.yml

si publish : TODO
	- pour chaque serveurs listé dans le fichier publish-targets.yml
	- pour chaque generated.build.* (mais juste public dans un premier temps)
	- copier le contenu dans le dossier distant indiqué, selon la méthode indiqué (dans publish-targets.yml)

si init : TODO
	- personnaliser selon une liste de questions à l'utilisateur (ou des paramètres ou un fichier de config pour pouvoir utiliser l'initialisation en non interactif)

 */
